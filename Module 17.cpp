#include <iostream>

using namespace std;

class Vector
{
private:

	double x;
	double y;
	double z;

public:

    Vector() : x(0), y(0), z(0){}

    //Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z){}

    void Show()
    {
        cout << "Your coordinates:\n";
        cout << "X:" << x << " Y:" << y << " Z:" << z << endl;
    }

    double GetLenght()
    {
         return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }
    
    void EnterCoordinates()
    {
        double* a = &x;
        double* b = &y;
        double* c = &z;

        cout << "Enter X:\n";
        cin >> *a;
        cout << "Enter Y:\n";
        cin >> *b;
        cout << "Enter Z:\n";
        cin >> *c;
        cout << endl;
    }
};

int main()
{
    cout << "Hello World!\n\n";
    
    Vector v;
    v.EnterCoordinates();
    v.Show();
    
    cout << "\n" << "Vector Lenght:" << v.GetLenght() << endl;
}
